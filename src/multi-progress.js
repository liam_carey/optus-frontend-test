﻿angular.module('test.MultiProgressBar', []);
angular.module('test.MultiProgressBar')
.controller('optusMultiProgressCtlr', ['$scope', function (scope) {
   var ctlr = this;

   this.bars = [];

   this.initBar = function (barScope, element) {
      this.bars.push(barScope);
   };

   scope.increment = function (amount) {
      var amount = Math.abs(amount);
      updateActiveBarValue(amount);
   };

   scope.decrement = function (amount) {
      var amount = -1 * Math.abs(amount);
      updateActiveBarValue(amount);
   };

   function updateActiveBarValue (amount) {
      var bar = ctlr.bars[scope.selectedBar],
            barValue = 0;

      if( bar!=null )
      {
         barValue = parseInt(bar.value, 10) + amount;

         if (barValue > 0)
            bar.value = barValue;
         else
            bar.value = 0;
      }
   };
}])
.directive('optusMultiProgress', function () {
   return {
      restrict: 'EA',
      scope: {
         bars: '=',
         width: '@'
      },
      template:'<div ng-repeat="bar in bars" data-progress-bar value="{{bar.value}}" width="{{width}}"></div>' +
               '<select ng-model="selectedBar">' +
               '   <option ng-repeat="bar in bars" value="{{$index}}">progress {{$index + 1}}</option>' +
               '</select>' +
               '<button id="minus25" ng-click="decrement(25)">-25</button>' +
               '<button id="minus10" ng-click="decrement(10)">-10</button>' +
               '<button id="plus10" ng-click="increment(10)">+10</button>' +
               '<button id="plus25" ng-click="increment(25)">+25</button>',
      controller: 'optusMultiProgressCtlr',
      link: function (scope, element, attrs) {
         scope.selectedBar = 0;
      }
   };
})
.directive('progressBar', function () {
   return {
      restrict: 'EA',
      scope: {
         value: '@',
         width: '@'
      },
      replace: true,
      transclude: true,
      require: '^optusMultiProgress',
      template: '<div class="delta-bar">' +
                   '<span>{{value}} %</span>' +
                   '<div></div>' +
                '</div>',
      link: function (scope, element, attrs, optusMultiProgressCtlr) {
         var percent = scope.value,
             width = scope.width,
             barWidth = 0;

         element.css({ 'width': width });
         element.children('span').css({ 'width': width });

         if (percent > 0)
         {
            setBarWidth();
            optusMultiProgressCtlr.initBar(scope, element);
         }
         
         scope.$watch('value', function (val) {
            setBarWidth(val);
         });

         function setBarWidth(barValue) {
            var innerBar = element.find('div');

            percent = (barValue > 0) ? barValue : 0;
            barWidth = Math.min(width, percent * width / 100);

            innerBar.animate({
               width: barWidth
            }, 500);

            if (percent >= 100)
               innerBar.addClass('red');
            else {
               if (innerBar.hasClass('red'))
                  innerBar.removeClass('red');

               if (percent >= 90)
                  innerBar.addClass('yellow');
               else if (innerBar.hasClass('yellow'))
                  innerBar.removeClass('yellow');
            }
         };
      }
   };
});