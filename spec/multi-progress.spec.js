﻿describe('Progress bar directive', function () {
   var element, scope;

   beforeEach(module('test.MultiProgressBar'));

   describe('Given a multi-progress directive', function () {
      beforeEach(inject(function ($rootScope, $compile) {
         scope = $rootScope.$new();
         scope.progressBars = [
            { value: 50 }, { value: 25 }, { value: 75 }
         ];
         element = $compile('<div optus-multi-progress bars="progressBars" width="400"></div>')(scope);
         scope.$root.$digest();
      }));

      function getBar(i) {
         return element.children('div.delta-bar').eq(i);
      }

      it('Should have 3 bars', function () {
         expect(element.find('div.delta-bar').length).toBe(3);
      });

      it('Should have 1st bar with text 50%', function () {
         var bar = getBar(0);
         expect(bar.children('span').text()).toBe('50 %');
      });

      it('Should have 1st bar with width 200', function (done) {
         var bar = getBar(0);
         setTimeout(function () {
            expect(bar.children('div').width()).toBe(200);
            done();
         }, 1000);
      });

      it('Should set selected bar to 3rd bar', function () {
         element.find('select').val(2).trigger('change');
         expect(element.isolateScope().selectedBar).toBe('2');
      });

      describe('Upon decrement 25% button click on 1st bar', function () {
         beforeEach(function () {
            element.find('#minus25').trigger('click');
         });
         it('Should have 1st bar with 25%', function () {
            var bar = getBar(0);
            expect(bar.children('span').text()).toBe('25 %');
         });
      });

      describe('Upon decrement 10% button click on 1st bar', function () {
         beforeEach(function () {
            element.find('#minus10').trigger('click');
         });
         it('Should have 1st bar with 40%', function () {
            var bar = getBar(0);
            expect(bar.children('span').text()).toBe('40 %');
         });
      });

      describe('Upon increment 10% button click on 2nd bar', function () {
         beforeEach(function () {
            element.find('select').val(1).trigger('change');
            element.find('#plus10').trigger('click');
         });
         it('Should have 2nd bar with 35%', function () {
            var bar = getBar(1);
            expect(bar.children('span').text()).toBe('35 %');
         });
      });

      describe('Upon increment 25% button click on 2nd bar', function () {
         beforeEach(function () {
            element.find('select').val(1).trigger('change');
            element.find('#plus25').trigger('click');
         });
         it('Should have 2nd bar with 50%', function () {
            var bar = getBar(1);
            expect(bar.children('span').text()).toBe('50 %');
         });
      });
   });
});